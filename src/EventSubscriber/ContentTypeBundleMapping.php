<?php

namespace Drupal\pepper_blog\EventSubscriber;

use Drupal\pepper_graphql\Event\ContentTypeBundleMappingEvent;
use Drupal\pepper_graphql\Event\PepperGraphqlEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ContentTypeBundleMapping.
 *
 * @package Drupal\pepper_blog\EventSubscriber
 */
class ContentTypeBundleMapping implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      PepperGraphqlEvents::CONTENT_TYPE_BUNDLE_MAPPING => 'PepperGraphqlContentTypeBundleMapping',
    ];
  }

  /**
   * Subscribe to the event dispatched.
   *
   * @param ContentTypeBundleMappingEvent $event
   *   Our ContentTypeBundleMappingEvent event object.
   */
  public function PepperGraphqlContentTypeBundleMapping(ContentTypeBundleMappingEvent $event) {
    $bundles = $event->getMapping();
    $bundles['blog_author'] =  'BlogAuthor';
    $bundles['blog_detailpage'] =  'BlogDetailpage';
    $bundles['blog_overview'] =  'BlogOverview';


    $event->setMapping($bundles);
  }

}

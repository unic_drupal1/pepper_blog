<?php

namespace Drupal\pepper_blog\Plugin\GraphQL\DataProducer;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *  Format date with drupal date format.
 *
 * @DataProducer(
 *   id = "pepper_date_formatter",
 *   name = @Translation("Producer to format date with drupal formats"),
 *   description = @Translation("Returns a formated date."),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("Format date"),
 *     required = FALSE
 *   ),
 *   consumes = {
 *     "timestamp" = @ContextDefinition("string",
 *       label = @Translation("Timestamp")
 *     ),
 *     "format" = @ContextDefinition("string",
 *       label = @Translation("Date format"),
 *     ),
 *   }
 * )
 */
class BlogDateFormatter extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * PepperRouteItems constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param $date_formatter
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DateFormatter $date_formatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('date.formatter'),
    );
  }

  /**
   * Resolver.
   *
   * @param string|null $format
   * @param $timestamp
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   The caching context related to the current field.
   * @return string|null
   *
   * @throws \Exception
   */
  public function resolve($timestamp, $format, FieldContext $context) {
    return $this->dateFormatter->format($timestamp, $format, NULL, NULL, $context->getContextLanguage());
  }

}

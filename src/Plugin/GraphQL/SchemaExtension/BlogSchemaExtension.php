<?php

namespace Drupal\pepper_blog\Plugin\GraphQL\SchemaExtension;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\PepperRouteLoad;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Schema extension.
 *
 * @SchemaExtension (
 *   id = "blog_schema",
 *   description = "Blog schema extension.",
 *   name = "Blog schema",
 *   schema = "custom_composable"
 * )
 */
class BlogSchemaExtension extends SdlSchemaExtensionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Maps the schema languageId ENUM to Drupal language ids.
   *
   * Note! This is not the mapping to the path prefix itself. Here we only map
   * the incoming ENUM from GraphQL to the language id of Drupal.
   *
   * @var array
   *
   * @see PepperRouteLoad::getPathPrefix
   */
  private $languageIdMapping = [
    'EN' => 'en',
    'DE' => 'de',
    'FR' => 'fr',
    'IT' => 'it',
  ];

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('language_manager')
    );
  }

  /**
   * HeadlessSchema constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin Id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Cache\CacheBackendInterface $astCache
   *   The cache bin for caching the parsed SDL.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\graphql\Plugin\SchemaExtensionPluginManager $extensionManager
   *   The schema extension plugin manager.
   * @param array $config
   *   The service configuration.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(
    array                    $configuration,
                             $pluginId,
                             $pluginDefinition,
    ModuleHandlerInterface   $moduleHandler,
    LanguageManagerInterface $languageManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $moduleHandler, $languageManager);
    $this->languageManager = $languageManager;
  }

  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();
    $this->addFieldResolversBlogOverview($registry, $builder);
    $this->addFieldResolversBlogDetailpage($registry, $builder);
    $this->addFieldResolversBlogAuthor($registry, $builder);
    $this->addFieldResolversBlogTeaserParagraph($registry, $builder);
  }

  public function addFieldResolversBlogOverview($registry, $builder) {

    $registry->addFieldResolver('BlogOverview', 'nid',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogOverview', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogOverview', 'bundle',
      $builder->produce('entity_bundle')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogOverview', 'title',
      $builder->produce('entity_label')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver(
      'BlogOverview',
      'langcode',
      $builder->compose(
        $builder->produce('entity_language')->map('entity', $builder->fromParent()),
        $builder->callback(function ($parent) {
          return $parent->getId();
        })
      )
    );

    $registry->addFieldResolver('BlogOverview', 'lead',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:node'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_lead.value'))
    );

    $registry->addFieldResolver('BlogOverview', 'author',
      $builder->compose(
        $builder->produce('entity_owner')
          ->map('entity', $builder->fromParent()),
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('BlogOverview', 'date_published',
      $builder->produce('entity_created')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogOverview', 'date_modified',
      $builder->produce('entity_changed')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogOverview', 'seo_information',
      $builder->produce('seo_information')
        ->map('input', $builder->fromParent())
        ->map('info', $builder->fromValue('metatags'))
    );

    $registry->addFieldResolver('BlogOverview', 'alternates',
      $builder->produce('entity_translations')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogOverview', 'breadcrumb',
      $builder->produce('pepper_breadcrumb')
        ->map('entity', $builder->fromParent())
        ->map('language', $builder->fromContext('language'))
    );

    $registry->addFieldResolver('BlogOverview', 'blog_view',
      $builder->produce('pepper_views_views_reference')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_blog_view'))
    );

    $registry->addFieldResolver('BlogOverview', 'url',
      $builder->compose(
        $builder->produce('entity_url')
          ->map('entity', $builder->fromParent()),
        $builder->produce('url_path')
          ->map('url', $builder->fromParent())
      )
    );
  }

  public function addFieldResolversBlogDetailpage($registry, $builder) {
    $registry->addFieldResolver('BlogDetailpage', 'nid',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogDetailpage', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogDetailpage', 'bundle',
      $builder->produce('entity_bundle')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogDetailpage', 'title',
      $builder->produce('entity_label')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogDetailpage', 'author',
      $builder->compose(
        $builder->produce('entity_owner')
          ->map('entity', $builder->fromParent()),
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver(
      'BlogDetailpage',
      'langcode',
      $builder->compose(
        $builder->produce('entity_language')->map('entity', $builder->fromParent()),
        $builder->callback(function ($parent) {
          return $parent->getId();
        })
      )
    );

    $registry->addFieldResolver('BlogDetailpage', 'date_published',
      $builder->compose(
        $builder->produce('property_path')
          ->map('type', $builder->fromValue('entity:node'))
          ->map('value', $builder->fromParent())
          ->map('path', $builder->fromValue('created.value')),
        $builder->produce('pepper_date_formatter')
          ->map('timestamp', $builder->fromParent())
          ->map('format', $builder->fromValue('blog_date_format'))
      )
    );

    $registry->addFieldResolver('BlogDetailpage', 'date_modified',
      $builder->compose(
        $builder->produce('property_path')
          ->map('type', $builder->fromValue('entity:node'))
          ->map('value', $builder->fromParent())
          ->map('path', $builder->fromValue('changed.value')),
        $builder->produce('pepper_date_formatter')
          ->map('timestamp', $builder->fromParent())
          ->map('format', $builder->fromValue('blog_date_format'))
      )
    );

    $registry->addFieldResolver('BlogDetailpage', 'lead',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:node'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_lead.value'))
    );

    $registry->addFieldResolver('BlogDetailpage', 'teaser_title', $builder->compose(
      $builder->callback(function ($value) {
        // Return title if teaser title is not set.
        if ($value instanceof Node) {
          if ($value->hasField('field_teaser_title') && !empty($value->get('field_teaser_title')
              ->getValue())) {
            return $value->get('field_teaser_title')->value;
          }
          else {
            return $value->getTitle();
          }
        }
      }
      )));


    $registry->addFieldResolver('BlogDetailpage', 'teaser_text', $builder->compose(
      $builder->callback(function ($value) {
        // Return lead if teaser text is not set.
        if ($value instanceof Node) {
          if ($value->hasField('field_teaser_text') && !empty($value->get('field_teaser_text')
              ->getValue())) {
            return $value->get('field_teaser_text')->value;
          }
          elseif ($value->hasField('field_lead') && !empty($value->get('field_lead')
              ->getValue())) {
            return $value->get('field_lead')->value;
          }
        }
      }
      )));

    $registry->addFieldResolver('BlogDetailpage', 'teaser_image', $builder->compose(
      $builder->callback(function ($value) {
        // Return hero image if teaser image is not set.
        if ($value instanceof Node) {
          if ($value->hasField('field_teaser_image') && !empty($value->get('field_teaser_image')
              ->getValue())) {
            $mid = $value->get('field_teaser_image')
              ->getValue()['0']['target_id'];
          }
          elseif ($value->hasField('field_blog_hero') && !empty($value->get('field_blog_hero')
              ->getValue())) {
            $mid = $value->get('field_blog_hero')->getValue()['0']['target_id'];
          }
          if (isset($mid)) {
            $media = Media::load($mid);
            if ($media instanceof Media) {
              return $media;
            }
          }
        }
      }
      )));


    // Returns a single entity for field_image.
    $registry->addFieldResolver('BlogDetailpage', 'image',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:node'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_blog_hero.entity'))
    );

    $registry->addFieldResolver('BlogDetailpage', 'site_name',
      $builder->produce('site_name')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogDetailpage', 'seo_information',
      $builder->produce('seo_information')
        ->map('input', $builder->fromParent())
        ->map('info', $builder->fromValue('metatags'))
    );

    $registry->addFieldResolver('BlogDetailpage', 'breadcrumb',
      $builder->produce('pepper_breadcrumb')
        ->map('entity', $builder->fromParent())
        ->map('language', $builder->fromContext('language'))
    );

    $registry->addFieldResolver('BlogDetailpage', 'alternates',
      $builder->produce('entity_translations')
        ->map('entity', $builder->fromParent())
    );
    $registry->addFieldResolver('BlogDetailpage', 'blog_view',
      $builder->produce('pepper_views_views_reference')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_blog_view'))
    );

    $registry->addFieldResolver('BlogDetailpage', 'categories',
      $builder->produce('entity_reference')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_categories'))
        ->map('language', $builder->fromContext('language'))
    );


    // Add fields to BlogDetailPage.
    $registry->addFieldResolver('BlogDetailpage', 'blog_author',
      $builder->produce('entity_reference')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_blog_author'))
        ->map('language', $builder->fromContext('language'))
    );

    $registry->addFieldResolver('BlogDetailpage', 'content_elements', $builder->compose(
      $builder->callback(function ($values) {
        if ($values instanceof Node) {
          $nodeLanguage = $values->language()->getId();
          if ($values->hasField('field_layout_paragraphs') && $paragraphs = $values->get('field_layout_paragraphs')
              ->referencedEntities()) {
            /**
             * @var int $index
             * @var ParagraphInterface $paragraph
             */
            $returned = [];
            foreach ($paragraphs as $index => $paragraph) {
              // Filter out disabled paragraphs.
              if ($paragraph->getBehaviorSetting('layout_paragraphs', 'region') == '_disabled') {
                continue;
              }
              $returned[$paragraph->getRevisionId()] = $paragraph;
              if ($paragraph->hasTranslation($nodeLanguage)) {
                $returned[$paragraph->getRevisionId()] = $paragraph->getTranslation($nodeLanguage);
              }
            }
            return $returned;
          }
        }
        return [];
      }),
      $builder->callback(function ($values) {
        return array_filter($values, function ($paragraph) {
          /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
          $parent_uuid = $paragraph->getBehaviorSetting('layout_paragraphs', 'parent_uuid');
          return empty($parent_uuid);
        });
      })
    ));

    $registry->addFieldResolver('BlogDetailpage', 'content_elements__preview', $builder->compose(
      $builder->callback(function ($values) {
        if ($values instanceof Node) {
          if ($serialized = \Drupal::service('tempstore.shared')
            ->get('decoupled_preview')
            ->get($values->uuid())) {
            /** @var NodeInterface $values */
            $values = unserialize($serialized);
            $nodeLanguage = $values->language()->getId();
            if ($values->hasField('field_layout_paragraphs') && $paragraphs = $values->get('field_layout_paragraphs')
                ->getValue()) {
              $paragraphs = array_column($paragraphs, 'entity');

              /**
               * @var int $index
               * @var ParagraphInterface $paragraph
               */
              foreach ($paragraphs as $index => $paragraph) {
                if ($paragraph->hasTranslation($nodeLanguage)) {
                  $paragraphs[$index] = $paragraph->getTranslation($nodeLanguage);
                }
              }
              return $paragraphs;
            }
          }
        }
        return [];
      }),
      $builder->callback(function ($values) {
        return array_filter($values, function ($paragraph) {
          /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
          $parent_uuid = $paragraph->getBehaviorSetting('layout_paragraphs', 'parent_uuid');
          return empty($parent_uuid);
        });
      })
    ));

    $registry->addFieldResolver('BlogDetailpage', 'url',
      $builder->compose(
        $builder->produce('entity_url')
          ->map('entity', $builder->fromParent()),
        $builder->produce('url_path')
          ->map('url', $builder->fromParent())
      )
    );

  }

  public function addFieldResolversBlogAuthor($registry, $builder) {
    $registry->addFieldResolver('BlogAuthor', 'nid',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogAuthor', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogAuthor', 'bundle',
      $builder->produce('entity_bundle')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogAuthor', 'title',
      $builder->produce('entity_label')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogAuthor', 'date_published',
      $builder->compose(
        $builder->produce('property_path')
          ->map('type', $builder->fromValue('entity:node'))
          ->map('value', $builder->fromParent())
          ->map('path', $builder->fromValue('created.value')),
        $builder->produce('pepper_date_formatter')
          ->map('timestamp', $builder->fromParent())
          ->map('format', $builder->fromValue('blog_date_format'))
      )
    );

    $registry->addFieldResolver('BlogAuthor', 'date_modified',
      $builder->compose(
        $builder->produce('property_path')
          ->map('type', $builder->fromValue('entity:node'))
          ->map('value', $builder->fromParent())
          ->map('path', $builder->fromValue('changed.value')),
        $builder->produce('pepper_date_formatter')
          ->map('timestamp', $builder->fromParent())
          ->map('format', $builder->fromValue('blog_date_format'))
      )
    );

    $registry->addFieldResolver('BlogAuthor', 'lead',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:node'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_lead.value'))
    );


    // Returns a single entity for field_image.
    $registry->addFieldResolver('BlogAuthor', 'image',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:node'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_image.entity'))
    );

    $registry->addFieldResolver('BlogAuthor', 'site_name',
      $builder->produce('site_name')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogAuthor', 'seo_information',
      $builder->produce('seo_information')
        ->map('input', $builder->fromParent())
        ->map('info', $builder->fromValue('metatags'))
    );

    $registry->addFieldResolver('BlogAuthor', 'alternates',
      $builder->produce('entity_translations')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogAuthor', 'url',
      $builder->compose(
        $builder->produce('entity_url')
          ->map('entity', $builder->fromParent()),
        $builder->produce('url_path')
          ->map('url', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('BlogAuthor', 'author',
      $builder->compose(
        $builder->produce('entity_owner')
          ->map('entity', $builder->fromParent()),
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );


  }

  public function addFieldResolversBlogTeaserParagraph($registry, $builder): void {
    $registry->addFieldResolver('BlogTeaser', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogTeaser', 'anchor_link',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('anchor_link.value'))
    );

    $registry->addFieldResolver('BlogTeaser', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogTeaser', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('BlogTeaser', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('BlogTeaser', 'blog_overview_page',
      $builder->produce('entity_reference')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_blog_overview_page'))
        ->map('language', $builder->fromContext('language'))
    );

    $registry->addFieldResolver('BlogTeaser', 'blog_view',
      $builder->produce('pepper_views_views_reference')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_blog_view'))
    );

  }

}
